--
-- File generated with SQLiteStudio v3.4.4 on จ. ก.ย. 18 00:04:46 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: catagory
DROP TABLE IF EXISTS catagory;

CREATE TABLE IF NOT EXISTS catagory (
    catagory_id   INTEGER   PRIMARY KEY,
    catagory_name TEXT (50) NOT NULL
);

INSERT INTO catagory (
                         catagory_id,
                         catagory_name
                     )
                     VALUES (
                         1,
                         'ขนม'
                     );

INSERT INTO catagory (
                         catagory_id,
                         catagory_name
                     )
                     VALUES (
                         2,
                         'กาแฟ'
                     );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE IF NOT EXISTS product (
    product_id             INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    produnct_name          TEXT (50) UNIQUE,
    product_price          DOUBLE    NOT NULL,
    product_size           TEXT (5)  DEFAULT SML
                                     NOT NULL,
    [product_sweet_level ] TEXT (5)  DEFAULT (123) 
                                     NOT NULL,
    product_type           TEXT (5)  DEFAULT HCF
                                     NOT NULL,
    cat_id                 INTEGER   DEFAULT (1) 
                                     NOT NULL
                                     REFERENCES catagory (catagory_id) ON DELETE RESTRICT
                                                                       ON UPDATE RESTRICT
);

INSERT INTO product (
                        product_id,
                        produnct_name,
                        product_price,
                        product_size,
                        [product_sweet_level ],
                        product_type,
                        cat_id
                    )
                    VALUES (
                        1,
                        'Esoresso',
                        30.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        produnct_name,
                        product_price,
                        product_size,
                        [product_sweet_level ],
                        product_type,
                        cat_id
                    )
                    VALUES (
                        2,
                        'Americano',
                        40.0,
                        'SML',
                        '012',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        produnct_name,
                        product_price,
                        product_size,
                        [product_sweet_level ],
                        product_type,
                        cat_id
                    )
                    VALUES (
                        3,
                        'เค้กชิฟฟ่อนช็อกโกแลต',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        produnct_name,
                        product_price,
                        product_size,
                        [product_sweet_level ],
                        product_type,
                        cat_id
                    )
                    VALUES (
                        4,
                        'บัตเตอร์เค้ก',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
